//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef INPUT_HANDLER__H
#define INPUT_HANDLER__H

#include <glass_types.h>
#include <functional>

class input_handler_t {
 public:
  input_handler_t() = default;
  input_handler_t(region_t region,
                  std::function<void(point_t, input_handler_t *)> button_press_fn,
                  std::function<void(point_t, input_handler_t *)> button_held_fn,
                  std::function<void(point_t, input_handler_t *)> button_release_fn,
                  std::function<void(point_t, input_handler_t *)> button_clear_fn);
  ~input_handler_t() = default;

  void handle_input(point_t point, bool mouse_down);
  void set_region(region_t region);
  region_t region();

  bool grabbed() { return m_grabbed; }
  void set_grabbed(bool grabbed) { m_grabbed = grabbed; }

 private:
  // --- Extra-guest mouse handling ---
  // A small state machine for handling guest input outside of guest render
  // targets:
  //
  //     +-----------------------------+<-------------+
  //     v                             |              |
  // +---------+    +---------+    +---------+    +---------+
  // | MButton |--->| MButton |    | MButton |--->| MButton |
  // | Press   |    | Held    |--->| Release |    | Clear   |
  // +---------+    +---------+    +---------+    +---------+
  //     |                             ^
  //     +-----------------------------+

  enum mbutton_state {
    mbutton_press,
    mbutton_held,
    mbutton_release,
    mbutton_clear
  } m_mbutton_state;

  region_t m_region;

  QString dump_state(mbutton_state state);
  void dump_state_transition(mbutton_state old_state, mbutton_state new_state);

  void process_mbutton_state_transitions(bool mouse_down);

  std::function<void(point_t, input_handler_t *)> m_button_press_function;
  std::function<void(point_t, input_handler_t *)> m_button_held_function;
  std::function<void(point_t, input_handler_t *)> m_button_release_function;
  std::function<void(point_t, input_handler_t *)> m_button_clear_function;

  // It's up to the lamda to clear these flags as
  // they see fit
  bool m_grabbed;
};

#endif // INPUT_HANDLER__H
