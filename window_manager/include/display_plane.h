//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DISPLAY_PLANE__H
#define DISPLAY_PLANE__H

#include <render_plane.h>
#include "framebuffer.h"
// See definitions in common/include/plane.h

class display_plane_t : public plane_t
{
public:
    display_plane_t(rect_t rect = rect_t(0, 0, 0, 0),
                    point_t plane_origin = QPoint(0, 0),
                    uint32_t banner_height = 25);
    virtual ~display_plane_t() = default;

    virtual transform_t from(plane_t &source_plane);
    virtual transform_t to(plane_t &source_plane);
    virtual transform_t translate(plane_t &source_plane);

    virtual point_t map_to(plane_t *source_plane, point_t point);
    virtual point_t map_from(plane_t *source_plane, point_t point);
  
    virtual void set_rect(rect_t rect);

    virtual render_plane_t &render_plane();
    virtual void show_cursor(std::shared_ptr<cursor_t> cursor) = 0;
    virtual void show_cursor() = 0;
    virtual void move_cursor(point_t point) = 0;
    virtual void hide_cursor() = 0;
    virtual bool cursor_visible() { return m_cursor_visible; }

    virtual void dpms_on() = 0;
    virtual void dpms_off() = 0;

    virtual uint32_t unique_id();
    virtual void set_unique_id(uint32_t unique_id);
    virtual uint32_t banner_height();

    virtual std::shared_ptr<framebuffer_t> framebuffer();
    virtual void operator+=(const rect_t &rect);

    virtual std::string name();
    void set_name(std::string name);

    virtual std::shared_ptr<QImage> scratch() { return m_scratch; }
    virtual region_t &clip() { return m_clip; }
    virtual region_t &current_clip() { return m_current_clip; }
    virtual region_t &last_clip() { return m_last_clip; }

protected:
    render_plane_t m_render_plane;
    std::shared_ptr<framebuffer_t> m_fb;
    std::shared_ptr<framebuffer_t> m_cursor;
    std::shared_ptr<QImage> m_scratch;

    region_t m_clip;
    region_t m_current_clip;
    region_t m_last_clip;

    uint32_t m_unique_id;
    uint32_t m_banner_height;

    std::string m_name;
    bool m_cursor_visible = false;
};

#endif // DISPLAY_PLANE__H
