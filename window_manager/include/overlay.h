//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef OVERLAY_H
#define OVERLAY_H

#include <QPainter>

#include <glass_types.h>
#include <input_plane.h>

class overlay_t
{
public:
    overlay_t(uint32_t display_id, region_t region) :
    m_display_id(display_id),
    m_updated(true),
    m_region(region),
    m_visible(false),
    m_update_count(0) {}

    overlay_t(region_t region) :
    m_updated(true),
    m_region(region),
    m_visible(false),
    m_update_count(0) {}

    virtual ~overlay_t() = default;

    // Non-virtual to avoid misbehaving overlays
    void render_overlay(QPainter &p,
                        desktop_plane_t *desktop,
                        display_plane_t *display,
                        region_t &display_clip,
                        region_t &painted_clip)
    {
        if(!m_visible) {
            return;
        }

        region_t subset_visible_region = desktop->map_to(display,
                                                         m_visible_region & display->parent_rect());
        display_clip += subset_visible_region;
        p.setClipRegion(subset_visible_region);
        render(p, desktop, display, display_clip, painted_clip);
        display_clip -= subset_visible_region;
        painted_clip += subset_visible_region;
        display->current_clip() += subset_visible_region;
    }

    virtual void process_input(point_t point, bool mouse_down)
    {
        (void)point;
        (void)mouse_down;
    }

    virtual uint32_t display_id()
    {
        return m_display_id;
    }

    virtual region_t &visible_region()
    {
        return m_visible_region;
    }

    virtual void clip_visible_region(region_t &clip)
    {
        m_visible_region &= clip;
    }

    virtual region_t &region()
    {
        return m_region;
    }

    virtual void set_region(region_t region)
    {
        m_region = region;
    }

    virtual void set_region(rect_t rect)
    {
        m_region = rect;
    }

    virtual bool visible()
    {
        return m_visible;
    }

    virtual void set_visible(bool visible)
    {
        m_visible = visible;
    }

    virtual bool updated()
    {
        return m_updated;
    }

    virtual void set_updated(bool updated)
    {
        m_updated = updated;
    }

    virtual margins_t & margins()
    {
        return m_margins;
    }

    virtual void add_region(region_t region)
    {
        m_region += region;
        m_visible_region += region;
    }

    virtual void add_rect(rect_t rect)
    {
        m_region += rect;
    }

    virtual void render(QPainter &p,
                        desktop_plane_t *desktop,
                        display_plane_t *display,
                        region_t &display_clip,
                        region_t &painted_clip) = 0;
protected:
    uint32_t m_display_id;
    bool m_updated;
    region_t m_visible_region;
    region_t m_region;

    bool m_visible;
    uint32_t m_update_count;

    margins_t m_margins;

};

#endif // OVERLAY_H
