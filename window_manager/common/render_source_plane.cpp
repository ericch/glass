//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <render_source_plane.h>

render_source_plane_t::render_source_plane_t(rect_t rect, point_t plane_origin) : plane_t(rect, plane_origin),
                                                                                  m_framebuffer(nullptr),
                                                                                  m_cursor(nullptr),
                                                                                  m_key(0)
{
}

std::shared_ptr<cursor_t>
render_source_plane_t::cursor()
{
    if (m_cursor && m_cursor->enabled()) {
        return m_cursor;
    }
    return nullptr;
}

std::shared_ptr<QImage>
render_source_plane_t::framebuffer()
{
    return m_framebuffer;
}

region_t
render_source_plane_t::dirty_region()
{
    return m_dirty_region;
}

void
render_source_plane_t::set_key(window_key_t key)
{
    m_key = key;
}
