//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "battery_overlay.h"

#include <QMargins>

battery_overlay_t::battery_overlay_t(uint32_t display_id, region_t region) :
    overlay_t(display_id, region),
    m_value(0),
    m_power(false),
    m_battery_icon("/etc/mosaic/batteryIcons.png")
{}

void
battery_overlay_t::process_updates(std::shared_ptr<framebuffer_t> display)
{
    if (!m_updated || !m_visible) {
        return;
    }

    rect_t visible_rect = rect_t(display->width() - 101, 0, 100, 25);
    m_visible_region = visible_rect;

    // Calculate the offsets
    if(m_power) {
        m_icon_offset.setX(28);
    } else {
        m_icon_offset.setX(0);
    }

    if (m_value > 87) {
        m_icon_offset.setY(0);
    } else if (m_value > 62) {
        m_icon_offset.setY(14);
    } else if (m_value > 37) {
        m_icon_offset.setY(28);
    } else if (m_value > 10) {
        m_icon_offset.setY(42);
    } else {
        m_icon_offset.setY(56);
    }

    m_icon_offset.setWidth(28);
    m_icon_offset.setHeight(14);

    m_icon_rect = rect_t(visible_rect.x(), visible_rect.y(),
                         visible_rect.width() / 2, visible_rect.height());
    m_value_rect = rect_t(visible_rect.x() + visible_rect.width() / 2, visible_rect.y(),
                          visible_rect.width() - visible_rect.width() / 2, visible_rect.height());
}

void
battery_overlay_t::render(QPainter &p,
                          desktop_plane_t *desktop,
                          display_plane_t *display,
                          region_t &display_clip,
                          region_t &painted_clip)
{
    (void) desktop;
    (void) display;
    QFont font("DejaVu Sans", 10);
    QFontMetrics fm(font);
    QPen old_pen = p.pen();
    rect_t battery_rect = rect_t(p.device()->width() - 101, 0, 100, 25);

    if (!m_visible) {
        return;
    }
    if (!m_updated) {
        display_clip -= m_visible_region;
        painted_clip += m_visible_region;
        display->current_clip() += m_visible_region;
        return;
    }
    m_updated = false;

    p.drawImage(m_icon_rect, m_battery_icon, m_icon_offset);

    p.setPen(Qt::black);
    p.setFont(font);
    p.drawText(m_value_rect,
               QString::number(m_value) + '%',
               QTextOption(Qt::AlignAbsolute|
                           Qt::AlignHCenter|
                           Qt::AlignVCenter));
    p.setPen(old_pen);
    display_clip -= m_visible_region;
    painted_clip += m_visible_region;
    display->current_clip() += m_visible_region;
}
