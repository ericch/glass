//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef SWITCHER_OVERLAY_H
#define SWITCHER_OVERLAY_H

#include <glass_types.h>
#include <overlay.h>
#include <vm_base.h>
#include <vm_region.h>
#include <QObject>

#define VM_GAP 10.0
#define ICON_WIDTH_MIN 80.0
#define ICON_WIDTH_MAX 164.0
#define ICON_GAP 5.0
#define FONT_HEIGHT 50
#define MAX_NUM_VMS 9
#define CONNECTOR_GROUP_WIDTH (MAX_NUM_VMS * ICON_WIDTH_MIN)

class vm_region_t;

struct vm_icon_t {
    QImage icon_image;
    QString name;

    rect_t icon_rect;
    point_t name_origin;
};

class switcher_overlay_t : public QObject, public overlay_t
{
    Q_OBJECT
public:
    switcher_overlay_t(uint32_t display_id, region_t region,
                       qhash_t<uuid_t, std::shared_ptr<vm_region_t>>& vm_list,
                       list_t<uuid_t>& vm_stack);
    virtual ~switcher_overlay_t() = default;
    void process_updates(std::shared_ptr<framebuffer_t> display);
    void render(QPainter &p,
                desktop_plane_t *desktop,
                display_plane_t *display,
                region_t &display_clip,
                region_t &painted_clip);

public slots:
    void set_highlighted_vm(uuid_t highlighted_vm);
    void show_overlay(void);
    void hide_overlay(void);

private:
    double m_icon_width;
    double m_icon_height;
    qhash_t<uuid_t, std::shared_ptr<vm_region_t>>& m_vm_list;
    list_t<uuid_t>& m_vm_stack;
    uuid_t m_highlighted_vm;

    rect_t m_switcher_rect;
    rect_t m_highlight_rect;
    list_t<vm_icon_t> m_vm_icons;
};

#endif //SWITCHER_OVERLAY_H
