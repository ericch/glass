QT += core dbus

include(../../../common_include.prx)

CONFIG += c++17

INCLUDEPATH += . ../

HEADERS += ../xenmgr_dbus.h
SOURCES += ../xenmgr_dbus.cpp

HEADERS += ../xenmgr_vm_dbus.h
SOURCES += ../xenmgr_vm_dbus.cpp

HEADERS += ../dbus_helpers.h
SOURCES += ../dbus_helpers.cpp

HEADERS += ../dbus_listener.h
SOURCES += ../dbus_listener.cpp

SOURCES += dbus_listener_main.cpp
