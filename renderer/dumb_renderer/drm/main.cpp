//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <QCoreApplication>
#include <drm_gpu.h>
#include <iostream>
#include <json.hpp>

int
main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    json j;
    std::cin >> j;
    std::unique_ptr<drm_gpu_t> first_gpu;
    for (auto gpu_config : j["gpus"]) {
        first_gpu = std::make_unique<drm_gpu_t>(gpu_config);
        // try {

        //        break;
        // } catch(std::exception &e) {
        // 	std::cout << e.what();
        // }
    }

    return app.exec();
}
