//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "dumb_renderer.h"
#include <QtCore/QCoreApplication>
#include <iostream>
#include <json.hpp>

int
main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    json j;
    std::cin >> j;
    dumb_renderer renderer(j);

    return app.exec();
}
