QT       += core gui dbus
QT	 -= opengl

include(../../common_include.prx)

TARGET = dumb_renderer
DESTDIR = $$VG_BASE_DIR/lib
TEMPLATE = lib

SOURCES += dumb_renderer.cpp
SOURCES += blit_engine.cpp
SOURCES += $$VG_BASE_DIR/common/dbus.cpp
SOURCES += vglass_dbus_proxy.cpp
SOURCES += pv_vm_render_factory.cpp
SOURCES += drm/drm_gpu.cpp
SOURCES += drm/drm_crtc.cpp
SOURCES += drm/drm_connector.cpp
SOURCES += drm/drm_mode.cpp
SOURCES += drm/dumb_fb.cpp
SOURCES += drm/backlight.cpp
SOURCES += pv_guest/pv_display_resource.cpp
SOURCES += pv_guest/pv_desktop_resource.cpp
SOURCES += pt_gpu/pt_gpu.cpp
SOURCES += pt_gpu/pt_display.cpp

HEADERS += dumb_renderer.h
HEADERS += blit_engine.h
HEADERS += $$VG_BASE_DIR/common/include/dbus.h
HEADERS += vglass_dbus_proxy.h
HEADERS += pv_vm_render_factory.h
HEADERS += drm/drm_gpu.h
HEADERS += drm/drm_crtc.h
HEADERS += drm/drm_connector.h
HEADERS += drm/drm_mode.h
HEADERS += drm/dumb_fb.h
HEADERS += drm/backlight.h
HEADERS += pv_guest/pv_display_resource.h
HEADERS += pv_guest/pv_desktop_resource.h
HEADERS += pv_guest/pv_vm_render.h
HEADERS += pt_gpu/pt_gpu.h
HEADERS += pt_gpu/pt_display.h
HEADERS += ../include/vm_render.h
HEADERS += ../include/renderer.h

DBUS_ADAPTORS += vglass_dbus_proxy
vglass_dbus_proxy.files = mil.af.secureview.vglass.xml
vglass_dbus_proxy.source_flags = -l vglass_dbus_proxy
vglass_dbus_proxy.header_flags = -l vglass_dbus_proxy -i vglass_dbus_proxy.h

INCLUDEPATH += "./"
INCLUDEPATH += "$$(STAGING_DIR_TARGET)/usr/include/libdrm"

LIBS += -lpvbackendhelper -livc -ldrm

target.path = /usr/lib
INSTALLS += target
