//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <cstdlib>
#include "vglass_dbus_proxy.h"
#include "vglass_adaptor.h"
#include "drm/drm_gpu.h"
#include "drm/backlight.h"
#include "dumb_renderer.h"

extern int32_t g_banner_height;
extern std::list<std::string> gpu_devices;

vglass_dbus_proxy::vglass_dbus_proxy() : m_dbus(std::make_unique<DBus>()) {
    QDBusConnection::systemBus().connect("mil.af.secureview.disman",
                                         "/mil/af/secureview/disman",
                                         "mil.af.secureview.disman",
                                         "configChanged",
                                         this, SLOT(handleConfigChanged(QString)));
    QDBusConnection::systemBus().connect("com.citrix.xenclient.input",
                                         "/",
                                         "com.citrix.xenclient.input",
                                         "idle_timeout",
                                         this, SLOT(handleIdleTimeout(QString)));

    m_dbus->registerService<VglassAdaptor>(this, "mil.af.secureview.vglass");
    m_dbus->registerObject(this, "/mil/af/secureview/vglass");

    QDBusMessage msg = QDBusMessage::createMethodCall("mil.af.secureview.disman", "/mil/af/secureview/disman", "mil.af.secureview.disman", "refresh");
    QDBusConnection::systemBus().send(msg);
}

vglass_dbus_proxy::~vglass_dbus_proxy() {
    QDBusConnection::systemBus().disconnect("com.citrix.xenclient.input",
                                            "/",
                                            "com.citrix.xenclient.input",
                                            "idle_timeout",
                                            this, SLOT(handleIdleTimeout(QString)));
    QDBusConnection::systemBus().disconnect("mil.af.secureview.disman",
                                            "/mil/af/secureview/disman",
                                            "mil.af.secureview.disman",
                                            "configChanged",
                                            this, SLOT(handleConfigChanged(QString)));

    m_dbus->unregisterObject("/mil/af/secureview/vglass");
    m_dbus->unregisterService("mil.af.secureview.vglass");
}

QString vglass_dbus_proxy::getConfig(void) {
    TRACE;

    emit hotplug();
    json config = public_get_config(gpu_devices);

    return QString::fromStdString(config.dump());
}

void vglass_dbus_proxy::handleConfigChanged(QString disconfig) {
    TRACE;

    json config;

    try {
        config = json::parse(disconfig.toStdString());
    } catch(...) {
        vg_debug() << "Failed to parse config";
        vg_debug() << disconfig;
    }

    if (!config.empty()) {
        emit config_changed(config);
    }
}

void vglass_dbus_proxy::handleIdleTimeout(const QString timerName) {
    TRACE;

    if (timerName == "screen-blanking") {
        setDpms(true);
    }
}

void vglass_dbus_proxy::setDpms(const bool on) {
    TRACE;

    if (true == on) {
        emit dpms_on();
    } else {
        emit dpms_off();
    }
}

uint32_t vglass_dbus_proxy::backlighting(void) const {
    TRACE;

    backlight_t backlight;
    uint32_t value = backlight.level();

    return value;
}

void vglass_dbus_proxy::setBacklighting(const uint32_t level) const {
    TRACE;

    backlight_t backlight;
    backlight.set_level(level);
}

void vglass_dbus_proxy::increase_brightness(void) const {
    TRACE;

    backlight_t backlight;
    backlight.increase(10);
}

void vglass_dbus_proxy::decrease_brightness(void) const {
    TRACE;

    backlight_t backlight;
    backlight.decrease(10);
}

void vglass_dbus_proxy::identify(const bool on, const QString name) {
  TRACE;

  (void) name;
  if (on) {
    emit identify_on();
  } else {
    emit identify_off();
  }
}

void vglass_dbus_proxy::show_text(const bool on, const QString name) {
  TRACE;

  if (on) {
    emit show_text_on(name.toStdString());
  } else {
    emit show_text_off(name.toStdString());
  }
}

void vglass_dbus_proxy::clear_text() {
  TRACE;

  emit clear_all_text();
}

void vglass_dbus_proxy::exit() {
    std::exit(0);
}
