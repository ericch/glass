//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PV_DISPLAY_RESOURCE_H
#define PV_DISPLAY_RESOURCE_H

#include <glass_types.h>

#include <QObject>

#include <render_source_plane.h>
#include <vm.h>

extern "C" {
#include <pv_display_backend_helper.h>
}

enum render_command_t {
  BLANK,
  COPYBLIT,
};

class pv_display_resource_t : public QObject, public render_source_plane_t
{
    Q_OBJECT
public:
    static void dirty_rectangle_request(struct pv_display_backend *display,
                                        uint32_t x,
                                        uint32_t y,
                                        uint32_t w,
                                        uint32_t h);

    static void move_cursor_request(struct pv_display_backend *display,
                                    uint32_t x,
                                    uint32_t y);

    static void update_cursor_request(struct pv_display_backend *display,
                                      uint32_t x_hot,
                                      uint32_t y_hot,
                                      uint32_t show);

    static void set_display_request(struct pv_display_backend *display,
                                    uint32_t w,
                                    uint32_t h,
                                    uint32_t stride);

    static void blank_display_request(struct pv_display_backend *display,
                                      uint32_t reason);

    static void error_handler(struct pv_display_backend *display);

    static void new_dirty_rect_connection(void *opaque, struct libivc_client *client);
    static void new_framebuffer_connection(void *opaque, struct libivc_client *client);
    static void new_cursor_image_connection(void *opaque, struct libivc_client *client);
    static void new_event_connection(void *opaque, struct libivc_client *client);

    void send_add_display(void);

    pv_display_resource_t(std::shared_ptr<pv_display_consumer> consumer,
                          uint32_t key,
                          uint32_t x,
                          uint32_t y,
                          uint32_t w,
                          uint32_t h,
                          qlist_t<uint32_t> port_list);
    virtual ~pv_display_resource_t();

    virtual render_command_t render_command();

    virtual void clear();

    virtual bool update();
    virtual bool update_cached();

    virtual bool requires_update() const;
    virtual struct pv_display_backend *display();

    virtual bool valid();

    qlist_t<uint32_t> port_list();

signals:
    // Public signals to external interfaces
    void dirty_rectangle_signal(rect_t dirty_rectangle);
    void move_cursor_signal(uint32_t key, point_t position);
    void update_cursor_signal(uint32_t key, point_t hot_spot, std::shared_ptr<cursor_t> cursor);
    void hide_cursor_signal(uint32_t key);
    void set_display_signal(QSize resolution, uint32_t stride);
    void render_source_plane_signal(uint32_t key);
    void remove_display_signal(std::shared_ptr<pv_display_backend> pv_display,
                                             uint32_t key,
                                             qlist_t<uint32_t> port_list);
    void blank_display_signal(bool isOff);
private slots:
    void finish_new_dirty_rect_connection(void *cli);
    void finish_new_framebuffer_connection(void *cli);
    void finish_new_cursor_image_connection(void *cli);
    void finish_new_event_connection(void *cli);
    void issue_add_display_call(uint32_t port0,
                                uint32_t port1,
                                uint32_t port2,
                                uint32_t port3);

    void retry_set_display();
    void add_dirty_rectangle(uint32_t x, uint32_t y, uint32_t w, uint32_t h);
    void move_cursor(uint32_t key, uint32_t x, uint32_t y);
    void update_cursor(uint32_t x_hot, uint32_t y_hot, uint32_t show);
    void set_display(uint32_t w, uint32_t h, uint32_t stride);
    void blank_display(uint32_t reason);
    void disconnect_display();

protected:
  render_command_t m_render_command;

private:
  // Shared
  std::shared_ptr<pv_display_consumer> m_consumer;

  // Owned
  std::shared_ptr<pv_display_backend> m_pv_display;

  uint32_t m_starting_port;
  qmutex_t m_lock;

  bool m_updated;
  bool m_requires_update;

  point_t m_cursor_hot_spot;
  bool m_cursor_visible;
  QSize m_cursor_resolution;

  uint32_t m_x;
  uint32_t m_y;

  uint32_t m_retry_width;
  uint32_t m_retry_height;
  uint32_t m_retry_stride;

  qlist_t<uint32_t> m_port_list;

  Q_DISABLE_COPY(pv_display_resource_t);
};

#endif // PV_DISPLAY_RESOURCE_H
