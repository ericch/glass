//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "pv_desktop_resource.h"
#include "pv_common.h"

#include <QCoreApplication>

void
pv_desktop_resource_t::capabilities_request(struct pv_display_consumer *consumer,
                                            struct dh_driver_capabilities *request)
{
    pv_desktop_resource_t *self = static_cast<pv_desktop_resource_t *>(consumer->get_driver_data(consumer));

    if (!self) {
        throw std::invalid_argument("Invalid desktop resource referenced.");
    }

    QMutexLocker locker(&self->m_lock);

    if(request->max_displays < self->m_max_display_count) {
        self->m_max_display_count = std::min(request->max_displays, self->m_max_display_count);
    }

    QMetaObject::invokeMethod(self, "publish_display_list", Qt::QueuedConnection);
}

void
pv_desktop_resource_t::advertise_list_request(struct pv_display_consumer *consumer,
                                              struct dh_display_advertised_list *advertised_list)
{
    pv_desktop_resource_t *self = static_cast<pv_desktop_resource_t *>(consumer->get_driver_data(consumer));

    if (!self) {
        throw std::invalid_argument("Invalid desktop resource referenced.");
    }

    QMutexLocker locker(&self->m_lock);
    uint32_t limit = std::min(self->m_display_info_count, advertised_list->num_displays);

    for (uint32_t i = 0; i < limit; i++) {
        struct dh_display_info *display_info = &(advertised_list->displays[i]);
        vg_info() << "----> [" << display_info->key << "] x: " << display_info->x << " y: " << display_info->y << " w: " << display_info->width << " h: " << display_info->height;

        QMetaObject::invokeMethod(self, "add_display", Qt::QueuedConnection,
                                  Q_ARG(uint32_t, display_info->key),
                                  Q_ARG(uint32_t, display_info->x),
                                  Q_ARG(uint32_t, display_info->y),
                                  Q_ARG(uint32_t, display_info->width),
                                  Q_ARG(uint32_t, display_info->height));
    }
}

void
pv_desktop_resource_t::display_no_longer_available_request(struct pv_display_consumer *consumer,
                                                           struct dh_display_no_longer_available *request)
{
    pv_desktop_resource_t *self =
        static_cast<pv_desktop_resource_t *>(consumer->get_driver_data(consumer));

    if (!self) {
        throw std::invalid_argument("Invalid desktop resource referenced.");
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self, "remove_display", Qt::QueuedConnection,
                              Q_ARG(uint32_t, request->key));
}

void
pv_desktop_resource_t::text_mode_request(struct pv_display_consumer *consumer,
                                                   bool force)
{
    pv_desktop_resource_t *self =
        static_cast<pv_desktop_resource_t *>(consumer->get_driver_data(consumer));

    if (!self) {
        throw std::invalid_argument("Invalid desktop resource referenced.");
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self, "enable_text_mode", Qt::QueuedConnection,
                              Q_ARG(bool, force));
}

void
pv_desktop_resource_t::fatal_consumer_error(struct pv_display_consumer *consumer)
{
    pv_desktop_resource_t *self =
        static_cast<pv_desktop_resource_t *>(consumer->get_driver_data(consumer));

    if (!self) {
        throw std::invalid_argument("Invalid desktop resource referenced.");
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self, "handle_error", Qt::QueuedConnection);
}

void
pv_desktop_resource_t::new_control_channel_connection(void *opaque,
                                                      struct libivc_client *client)
{
    (void) client;
    pv_desktop_resource_t *self =
        static_cast<pv_desktop_resource_t *>(opaque);

    if (!self) {
        throw std::invalid_argument("Invalid desktop resource referenced.");
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self,
                              "finish_control_connection",
                              Qt::QueuedConnection,
                              Q_ARG(void *, (void *)client));
}

void
pv_desktop_resource_t::finish_control_connection(void *cli)
{
    struct libivc_client *client = (struct libivc_client *)cli;
    if(m_pv_backend_consumer) {
        m_pv_backend_consumer->finish_control_connection(m_pv_backend_consumer.get(), client);
    }
}

pv_desktop_resource_t::pv_desktop_resource_t(uuid_t uuid,
                                             domid_t domid,
                                             desktop_plane_t *desktop,
                                             uint32_t starting_port) :
  m_uuid(uuid),
  m_domid(domid),
  m_starting_port(starting_port),
  m_desktop(desktop)
{
    initialize();
}

pv_desktop_resource_t::~pv_desktop_resource_t()
{
    vg_info() << "Tearing down " << (qemu() ? "Qemu desktop: [" : "PV desktop: [") << m_domid << "] - " << m_uuid;
}

void
pv_desktop_resource_t::initialize()
{
    Expects(m_desktop);
    Expects(m_domid != 0);
    qRegisterMetaType<rect_t>("rect_t");
    int rc = 0;

    struct pv_display_consumer *consumer = nullptr;
    rc = create_pv_display_consumer(&consumer, m_domid, m_starting_port, (void*)this);

    if (rc || !consumer) {
        throw std::runtime_error("Failed to create pv backend.");
    }

    m_pv_backend_consumer = std::shared_ptr<pv_display_consumer>(consumer,
                                                                 destroy_pv_display_consumer);

    m_pv_backend_consumer->register_control_connection_handler(m_pv_backend_consumer.get(),
                                                               &pv_desktop_resource_t::new_control_channel_connection);

    m_pv_backend_consumer->register_driver_capabilities_request_handler(m_pv_backend_consumer.get(),
                                                                        &pv_desktop_resource_t::capabilities_request);

    // Register a display advertised list request handler.
    m_pv_backend_consumer->register_display_advertised_list_request_handler(m_pv_backend_consumer.get(),
                                                                            &pv_desktop_resource_t::advertise_list_request);

    // Register an display no longer available handler.
    m_pv_backend_consumer->register_display_no_longer_available_request_handler(m_pv_backend_consumer.get(),
                                                                                &pv_desktop_resource_t::display_no_longer_available_request);

    // Register a text mode handler
    m_pv_backend_consumer->register_text_mode_request_handler(m_pv_backend_consumer.get(),
                                                              &pv_desktop_resource_t::text_mode_request);

    // Register an Fatal Error handler
    m_pv_backend_consumer->register_fatal_error_handler(m_pv_backend_consumer.get(),
                                                        &pv_desktop_resource_t::fatal_consumer_error);

#define port_range 81
    m_display_info_count = 0;
    m_max_display_count = 7;
    for(uint32_t port = 1; port < port_range; port++) {
        m_port_pool.enqueue(m_starting_port + port);
    }

    vg_info() << DTRACE << m_port_pool;

    rc = m_pv_backend_consumer->start_server(consumer);
    if(rc) {
        throw std::runtime_error("Failed to create control server for pvdesktop");
    }
}

void
pv_desktop_resource_t::handle_error()
{
    emit desktop_ready(false);

    if(m_starting_port != 1500) {
        publish_display_list();
    }
}

dh_display_info *
pv_desktop_resource_t::display_infos()
{
    return m_display_info.get();
}

void
pv_desktop_resource_t::update_displays()
{
    uint32_t i = 0;

    if(!m_desktop) {
        return;
    }

    list_t<std::unique_ptr<render_target_plane_t>> &render_targets = m_desktop->render_targets(m_uuid);
    qlist_t<uint32_t> old_keys;
    qlist_t<uint32_t> current_keys;

    for(uint32_t j = 0; j < m_display_info_count; j++) {
        if(m_display_info_count > 0) {
            old_keys.push_back(m_display_info[j].key);
        }
    }

    for (auto &render_target : render_targets) {
        if(!render_target) {
            continue;
        }

        current_keys.push_back(render_target->key());
    }

    for(auto old_key : old_keys) {
        if(!current_keys.contains(old_key)) {
            remove_display(old_key);
        }
    }

    m_display_info = std::move(std::make_unique<dh_display_info[]>(render_targets.size()));

    for (auto &render_target : render_targets) {
        if(!render_target) {
            continue;
        }

        display_plane_t *display = m_desktop->display(render_target->key());

        if(i < m_max_display_count) {
            m_display_info[i].key = render_target->key();
            if(display) {
                m_display_info[i].x = display->origin().x();
                m_display_info[i].y = display->origin().y();
                if (display->origin().y() > (int32_t)display->banner_height()){
                    m_display_info[i].y -= display->banner_height();
                }
            } else {
                m_display_info[i].x = 0;
                m_display_info[i].y = 0;
            }
            m_display_info[i].width = render_target->rect().width();
            m_display_info[i].height = render_target->rect().height();
            i++;

            if(m_display_info[i].key == 1) {
                m_display_info_count = i;
                return;
            }
        } else {
            m_display_info_count = i;
            return;
        }
    }

    m_display_info_count = i;
}

void
pv_desktop_resource_t::update_displays(desktop_plane_t *desktop)
{
    m_desktop = desktop;
    publish_display_list();
}

void
pv_desktop_resource_t::publish_display_list()
{
    int rc = 1;

    if(!m_pv_backend_consumer) {
        return;
    }

    update_displays();

    if(m_display_info_count != 0) {
        rc = m_pv_backend_consumer->display_list(m_pv_backend_consumer.get(),
                                                 m_display_info.get(),
                                                 m_display_info_count);
    } else {
        if(m_desktop && !m_desktop->renderable()) {
            return;
        }

        vg_debug() << "No display infos allocated, try again in a second..";
    }

    if(rc && rc != -ENOENT) {
        QTimer::singleShot(1000, this, SLOT(publish_display_list()));
    }
}

void
pv_desktop_resource_t::connect_display_resource_signals(pv_display_resource_t *display)
{
    Qt::ConnectionType unique_direct = static_cast<Qt::ConnectionType>(Qt::DirectConnection | Qt::UniqueConnection);
    Qt::ConnectionType unique_queued = static_cast<Qt::ConnectionType>(Qt::QueuedConnection | Qt::UniqueConnection);

    QObject::connect(display,
                     &pv_display_resource_t::render_source_plane_signal,
                     this,
                     &pv_desktop_resource_t::render_source_plane_signal, unique_queued);
    QObject::connect(display,
                     &pv_display_resource_t::blank_display_signal,
                     this,
                     &pv_desktop_resource_t::blank_display_signal, unique_queued);
    QObject::connect(display,
                     &pv_display_resource_t::dirty_rectangle_signal,
                     this,
                     &pv_desktop_resource_t::add_dirty_rect, unique_queued);
    QObject::connect(display,
                     &pv_display_resource_t::move_cursor_signal,
                     this,
                     &pv_desktop_resource_t::move_cursor, unique_queued);
    QObject::connect(display,
                     &pv_display_resource_t::update_cursor_signal,
                     this,
                     &pv_desktop_resource_t::update_cursor, unique_queued);
    QObject::connect(display,
                     &pv_display_resource_t::hide_cursor_signal,
                     this,
                     &pv_desktop_resource_t::hide_cursor, unique_queued);
    QObject::connect(display,
                     &pv_display_resource_t::remove_display_signal,
                     this,
                     &pv_desktop_resource_t::remove_display_slot, unique_direct);
}

void pv_desktop_resource_t::disconnect_display_resource_signals(pv_display_resource_t *display)
{
    QObject::disconnect(display,
                        &pv_display_resource_t::render_source_plane_signal,
                        this,
                        &pv_desktop_resource_t::render_source_plane_signal);
    QObject::disconnect(display,
                        &pv_display_resource_t::blank_display_signal,
                        this,
                        &pv_desktop_resource_t::blank_display_signal);
    QObject::disconnect(display,
                        &pv_display_resource_t::dirty_rectangle_signal,
                        this,
                        &pv_desktop_resource_t::add_dirty_rect);
    QObject::disconnect(display,
                        &pv_display_resource_t::move_cursor_signal,
                        this,
                        &pv_desktop_resource_t::move_cursor);
    QObject::disconnect(display,
                        &pv_display_resource_t::update_cursor_signal,
                        this,
                        &pv_desktop_resource_t::update_cursor);
    QObject::disconnect(display,
                        &pv_display_resource_t::hide_cursor_signal,
                        this,
                        &pv_desktop_resource_t::hide_cursor);
    QObject::disconnect(display,
                        &pv_display_resource_t::remove_display_signal,
                        this,
                        &pv_desktop_resource_t::remove_display_slot);
}

void
pv_desktop_resource_t::add_display(uint32_t key,
                                   uint32_t x, uint32_t y,
                                   uint32_t w, uint32_t h)
{
    QMutexLocker locker(&m_lock);

    bool qemu = (m_starting_port == 1500);

    if(!m_desktop) {
        return;
    }

    for(auto &render_target : m_desktop->render_targets(m_uuid)) {
        if(!render_target) {
            continue;
        }

        if(render_target->key() == key && !render_target->pv_render_source()) {
            qlist_t<uint32_t> ports;

            for(int i = 0; i < 4; i++) {
                ports << m_port_pool.dequeue();
            }

            std::shared_ptr<render_source_plane_t> display =
                std::make_shared<pv_display_resource_t>(m_pv_backend_consumer, key,
                                                        x, y,
                                                        w, h,
                                                        ports);

            pv_display_resource_t *raw_display =
                static_cast<pv_display_resource_t *>(display.get());
            connect_display_resource_signals(raw_display);

            render_target->attach_render_source(display, qemu);
            if (qemu) {
                m_desktop->set_qemu_source(m_uuid,display);
            }
            emit desktop_ready(!qemu);
        } else if (render_target->key() == key && render_target->pv_render_source()) {
            //Are we updating the guest layout.
            auto rsp = render_target->pv_render_source();
            if (rsp->origin() != point_t(x, y))
            {
                rsp->set_origin(point_t(x, y));
                emit render_source_plane_signal(render_target->key());
            }
            else {
                vg_debug() << "Display exists, keys match, send add_display to have guest reconnect";
                pv_display_resource_t *display = static_cast<pv_display_resource_t *>(render_target->pv_render_source());
                if (display) {
                    display->send_add_display();
                }
            }
        } else {
            vg_debug() << "Display already exists, key mismatch, don't send add_display";
        }
    }
}

void
pv_desktop_resource_t::remove_display(uint32_t key)
{
    QMutexLocker locker(&m_lock);
    render_target_plane_t *rtp = nullptr;

    if(!m_desktop) {
        return;
    }

    for (auto &render_target : m_desktop->render_targets(m_uuid)) {
        if (!render_target) {
            continue;
        }

        if (render_target->key() == key && render_target->render_source() != nullptr) {
            rtp = render_target.get();
            break;
        }
    }

    if (rtp && rtp->render_source()) {

        if (rtp->render_source() == rtp->pv_render_source()) {

            //this pv_render_source is no longer valid, disconnect and remove
            pv_display_resource_t *display = static_cast<pv_display_resource_t *>(rtp->render_source());
            rtp->attach_render_source(nullptr, false);
            disconnect_display_resource_signals(display);

            //reattach and connect the qemu source
            m_desktop->attach_qemu_source(m_uuid);
            rtp->set_key(1);
            display = static_cast<pv_display_resource_t *>(rtp->render_source());
            connect_display_resource_signals(display);
            emit restore_qemu_signal();
        }
    }
    vg_info() << DTRACE << key;
}

void
pv_desktop_resource_t::remove_display_slot(std::shared_ptr<pv_display_backend> pv_display,
                                           uint32_t key,
                                           qlist_t<uint32_t> port_list)
{
    (void) key;
    (void)pv_display;

    for(auto port : port_list) {
        m_port_pool.enqueue(port);
    }
}

void
pv_desktop_resource_t::enable_text_mode(bool force)
{
    emit desktop_ready(force);
}
