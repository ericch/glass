//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef INPUT_DBUS_PROXY__H
#define INPUT_DBUS_PROXY__H

#include <QtCore>
#include <glass_types.h>
#include <dbus.h>
#include <dbwatch.h>


/**
 * XXX This is OpenXT specific, and definitely does not belong in the input server.
 */
#define KEYMAP_CONF_FILE "/config/keyboard.conf"
#define KEYMAP_LIST_FILE "/usr/share/xenclient/keyboards"
#define KEYMAP_LIST_MAX 1024

class db_dbus_t;
class input_server_t;
class input_dbus_proxy : public QObject {
    Q_OBJECT;

public:

    input_dbus_proxy(input_server_t* input_server);
    virtual ~input_dbus_proxy();

public slots:

    //
    // Internal Methods
    //
    void watched_value_changed(const QString &path);
    void add_pointer_device();

    //
    // DBus Methods
    //      this stuff is still supported
    //

    QStringList get_kb_layouts();
    QString get_current_kb_layout();
    void set_current_kb_layout(const QString &layout);

    int get_focus_domid();
    bool switch_focus(const int domid, const bool force);

    int get_mouse_speed();
    void set_mouse_speed(const int mouse_speed);

    bool get_tap_input();
    void set_tap_input(const bool enabled);
    void clear_tap_input();

    QString touchpad_get(const QString &prop);
    void touchpad_set(const QString &prop, const QString &value);

    void update_idle_timer(const QString &timer_name, int timeout);

    bool get_seamless_mousing();
    bool set_seamless_mousing(const bool enabled);

    bool reload_config_from_disk();

    //
    //  DBus JUNK
    //      not supported anymore, but too integrated with xenmgr to easily remove
    //

    bool auth_begin();
    void auth_clear_status();
    void auth_collect_password();
    void auth_create_hash(const QString &fname, const QString &password);
    QString auth_get_context(const QString &title, int flags);
    QString auth_get_status(bool clear, int &flags);
    void auth_remote_login(const QString &username, const QString &password);
    void auth_remote_status(bool auto_started, int status, const QString &id, const QString &username, const QString &recovery_key_file, uint ctx_flags);
    bool auth_rm_platform_user(QString &error_msg);
    void auth_set_context(const QString &user, const QString &title);
    void auth_set_context_flags(const QString &user, const QString &title, int flags);
    QString auth_title();
    void divert_keyboard_focus(const QString &uuid);
    void divert_mouse_focus(const QString &uuid, uint sframe_x1, uint sframe_y1, uint sframe_x2, uint sframe_y2, uint dframe_x1, uint dframe_y1, uint dframe_x2, uint dframe_y2);
    void focus_mode(int mode);
    bool get_auth_on_boot();
    int get_idle_time();
    int get_last_input_time();
    uint get_lid_state();
    QString get_platform_user(int &flags);
    QString get_remote_user_hash(const QString &userid);
    QString get_user_keydir(const QString &user);
    void lock(bool can_switch_out);
    int lock_timeout_get();
    void lock_timeout_set(int value);
    void set_auth_on_boot(bool auth);
    void set_slot(int domid, int slot);
    void stop_keyboard_divert();
    void stop_mouse_divert();
    void touch(const QString &uuid);
    void update_seamless_mouse_settings(const QString &dom_uuid);

signals:

    //
    // Internal Signals
    //
    void set_touchpad_tap_to_click(bool enabled);
    void set_touchpad_scrolling(bool enabled);
    void set_touchpad_speed(float touchpad_speed);
    void set_mouse_speed_f(float mouse_speed);
    void idle_timer_update(const QString &timer_name, int timeout);

    void set_tap_input_f(bool enabled);
    void clear_tap_input_f();

    //
    // DBus Signals
    //
    void keyboard_focus_change(const QString &uuid);
    void idle_timeout(const QString &timerName);

private:

    std::unique_ptr<DBus> m_dbus;
    std::unique_ptr<db_dbus_t> m_db;
    std::unique_ptr<db_watch> m_dbw;
    input_server_t* m_is;
};

#endif // INPUT_DBUS_PROXY__H
