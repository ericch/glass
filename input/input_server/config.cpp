//
// Config
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ===========================================================================
// Includes
// ============================================================================

#include <stdexcept>    // std::invalid_argument
#include <memory>       // std::make_unique
#include <QtCore>
#include <QDBusConnection>
#include <json.hpp>
#include <config.h>
#include <db_interface.h>

// ============================================================================
// Implementation
// ============================================================================

Config::Config() {
    db = std::make_unique<db_dbus_t>(QString("com.citrix.xenclient.db"),
                                     QString("/"),
                                     QDBusConnection::systemBus());
}

Config::~Config(void) {}

nlohmann::json Config::parse(const QString config) {
    json j;

    try {
        j = json::parse(config.toStdString());
    } catch (const std::invalid_argument& e) {
        qWarning() << "Failed to parse -" << config;
        qWarning() << e.what();
    }

    return j;
}

bool Config::exists(const QUrl url) const {
    if (!url.isValid()) {
        qWarning() << "configExists() failed - URL appears to be invalid.";
        return false;
    }

    QString scheme = url.scheme();
    QString path = url.path();
    if (url.authority().isEmpty() && !path.isEmpty() && !scheme.isEmpty() && (scheme == "file" || scheme == "db" || scheme == "rel")) {
        if (scheme == "rel") {
            path = path.mid(1);
        }

        qDebug() << "configExists() -" << scheme << path;

        if (scheme == "db") {
            return db->exists(path);
        } else { // if ((scheme == "file") || (scheme == "rel"))
            return QFile::exists(path);
        }
    }

    qWarning() << "configExists() failed - URL is incomplete or doesn't use a valid scheme.";
    return false;
}

bool Config::save(const QUrl url, const json &config) const {
    if (!url.isValid()) {
        qWarning() << "saveConfig() failed - URL appears to be invalid.";
        return false;
    }

    QString scheme = url.scheme();
    QString path = url.path();
    if (url.authority().isEmpty() && !path.isEmpty() && !scheme.isEmpty() && (scheme == "file" || scheme == "db" || scheme == "rel")) {
        if (scheme == "rel") {
            path = path.mid(1);
        }

        qDebug() << "saveConfig() -" << scheme << path;

        if (scheme == "db") {
            return saveConfigToDb(path, config);
        } else { // if ((scheme == "file") || (scheme == "rel"))
            return saveConfigToDisk(path, config);
        }
    }

    qWarning() << "saveConfig() failed - URL is incomplete or doesn't use a valid scheme.";
    return false;
}

nlohmann::json Config::load(const QUrl url) const {
    json j;

    if (!url.isValid()) {
        qWarning() << "loadConfig() failed - URL appears to be invalid, not able to load.";
        return json();
    }

    QString scheme = url.scheme();
    QString path = url.path();
    if (url.authority().isEmpty() && !path.isEmpty() && !scheme.isEmpty() && (scheme == "file" || scheme == "db" || scheme == "rel")) {

        if (scheme == "rel") {
            path = path.mid(1);
        }

        qDebug() << "loadConfig() -" << scheme << path;

        if (scheme == "db") {
            if (!db->exists(path)) {
                qWarning().nospace() << "The db path (" << path << ") doesn't exist.";
            } else {
                j = loadConfigFromDb(path);
            }
            return j;
        } else { // if ((scheme == "file") || (scheme == "rel"))
            if (!QFile::exists(path)) {
                qWarning().nospace() << "The file (" << path << ") doesn't exist.";
            } else {
                j = loadConfigFromDisk(path);
            }
            return j;
        }
    }

    qWarning() << "loadConfig() failed - URL is incomplete or doesn't use a valid scheme.";
    return j;
}

bool Config::remove(const QUrl url) const {
    if (!url.isValid()) {
        qWarning() << "removeConfig() failed - URL appears to be invalid.";
        return false;
    }

    QString scheme = url.scheme();
    QString path = url.path();
    if (url.authority().isEmpty() && !path.isEmpty() && !scheme.isEmpty() && (scheme == "file" || scheme == "db" || scheme == "rel")) {
        if (scheme == "rel") {
            path = path.mid(1);
        }

        qDebug() << "removeConfig() -" << scheme << path;

        if (scheme == "db") {
            db->rm(path);
            return true;
        } else { // if ((scheme == "file") || (scheme == "rel"))
            return QFile::remove(path);
        }
    }

    qWarning() << "removeConfig() failed - URL is incomplete or doesn't use a valid scheme.";
    return false;
}

// RCF - This is broke.
//      Should be creating the directory to ensure it's existance
//      Can't save to /blah and then /blah/wtf
bool Config::saveConfigToDisk(const QString path, const json &config) const {
    if (path.isEmpty()) {
        return false;
    }

    QFile file(path);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return false;
    }

    QTextStream out(&file);
    out << QString::fromStdString(config.dump(4));

    return true;
}

bool Config::saveConfigToDb(const QString path, const json &config) const {
    if (path.isEmpty()) {
        return false;
    }

    db->rm(path);
    db->inject(path, QString::fromStdString(config.dump()));
    return true;
}

nlohmann::json Config::loadConfigFromDisk(const QString path) const {
    if (path.isEmpty()) {
        return json();
    }

    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return json();
    }

    QTextStream in(&file);
    QString config = in.readAll();

    return Config::parse(config);
}

nlohmann::json Config::loadConfigFromDb(const QString path) const {
    if (path.isEmpty()) {
        return json();
    }

    QString config = db->dump(path);

    return Config::parse(config);
}
