//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <pv_vm_input.h>

pv_vm_input_t::pv_vm_input_t(std::shared_ptr<vm_base_t> vm, window_manager_t &wm) : vm_input_t(vm), m_wm(wm), m_active_sink(nullptr), m_led_code(0)
{
    CLEAR_ALL_KEYS(m_key_state);

    m_vkbd_sink = std::make_shared<vkbd_sink_t>(vm->domid(), 1);

    QObject::connect(m_vkbd_sink.get(), &vkbd_sink_t::connection_status,
                     this, &pv_vm_input_t::vkbd_connection_status, Qt::QueuedConnection);
    QObject::connect(m_vkbd_sink.get(), &guest_input_sink_t::keyboard_led_changed,
                     this, &vm_input_t::keyboard_led_changed);
    m_sinks.push_back(m_vkbd_sink);

    if(domid_t(vm->stub_domid()) < domid_t(DOMID_FIRST_RESERVED)) {
        m_qemu2_sink = std::make_shared<qemu2_sink_t>(vm->stub_domid());

        QObject::connect(m_qemu2_sink.get(), &guest_input_sink_t::keyboard_led_changed,
                         this, &vm_input_t::keyboard_led_changed);
        m_sinks.push_back(m_qemu2_sink);
        m_active_sink = m_qemu2_sink;

        if(m_vkbd_sink->connected())
        {
            m_active_sink = m_vkbd_sink;
        }
    } else {
        m_active_sink = m_vkbd_sink;
    }
}

pv_vm_input_t::~pv_vm_input_t()
{
    TRACE;
    m_sinks.clear();
}

void
pv_vm_input_t::keyboard_led_changed(const uint32_t led_code)
{
    m_led_code = led_code;
    emit set_input_led_code(m_led_code);
}

void clamp_point(rect_t rect, point_t &p)
{
    if(p.x() < rect.x()) {
        p.setX(rect.x());
    }

    if(p.x() > rect.topRight().x()) {
        p.setX(rect.topRight().x());
    }

    if(p.y() < rect.y()) {
        p.setY(rect.y());
    }

    if(p.y() > rect.bottomRight().y()) {
        p.setY(rect.bottomRight().y());
    }
}

static bool mouse_down = false;

void
pv_vm_input_t::transform_event(xt_input_event *event)
{
    uuid_t uuid = base()->uuid();
    point_t &hotspot = m_wm.input_plane().hotspot();
    rect_t input_rect = m_wm.input_plane().rect();
    bool is_touch = false;
    bool touch_down = false;

    //ok, so we use the hotspot to make an absolute point...
    switch(event->type) {
        case XT_TYPE_RELATIVE:
        {
            int32_t rx = lround(event->data.relative.relX);
            int32_t ry = lround(event->data.relative.relY);
            hotspot += point_t(rx, ry);
            break;
        }
        case XT_TYPE_ABSOLUTE:
        {
            hotspot = point_t(event->data.absolute.absX, event->data.absolute.absY);

            // If the hotspot was 0, 0 then set the hotspot to the
            // previous hotspot
            if (hotspot.isNull()) {
                hotspot = m_last_hotspot;
            }
            break;
        }
        case XT_TYPE_TOUCH_DOWN:
        case XT_TYPE_TOUCH_MOVE:
            touch_down = true;
        [[gnu::fallthrough]];
        case XT_TYPE_TOUCH_UP:
        {
            is_touch = true;

            //set new hotspot with point. this may jump with multiple slots?
            hotspot = point_t(event->data.touch.absX, event->data.touch.absY);

            vg_debug() << "pv_vm_input::transformevent type= " << event->type << " touchX,Y= " << event->data.touch.absX << ", " << event->data.touch.absY;

            // TOUCH_UP events have coords of 0,0 -- populate them so they find their way to the correct sink.
            if (hotspot.isNull()) {
                hotspot = m_last_hotspot;
            }
            break;
        }
        case XT_TYPE_TOOL_TIP:
            touch_down = event->data.tablet_tool.flags | XT_TYPE_TOOL_FLAG_TIP;
        [[gnu::fallthrough]];
        case XT_TYPE_TOOL_AXIS:
        case XT_TYPE_TOOL_PROXIMITY:
        {
            is_touch = true;
            hotspot = point_t(event->data.tablet_tool.absX, event->data.tablet_tool.absY);
            break;
        }
        case XT_TYPE_TOOL_BUTTON:
            return;
        default:
            return;
    }
    m_last_hotspot = hotspot;

    guest_mouse_event translated(point_t(-1, -1), QSize(0, 0));
    if (is_touch) {
        std::shared_ptr<display_plane_t> display_mt;
        std::shared_ptr<desktop_plane_t> desktop_mt;

        // Displays with an embedded displayport always begin with "eDP".
        std::string eDP("eDP");
        for (std::shared_ptr<desktop_plane_t> desktop : m_wm.input_plane().desktops())
            for (std::shared_ptr<display_plane_t> display : desktop->displays())
                if (display->name().compare(0, eDP.length(), eDP) == 0) {
                    desktop_mt = desktop;
                    display_mt = display;
                    break;
                }

        if (display_mt) {
            // Scale touch input using the display rect instead of the entire input rect and then translate it to be relative to the origin of the input rect.
            hotspot.setX(hotspot.x() * display_mt->render_plane().rect().width() / input_rect.width() + display_mt->origin().x() + desktop_mt->origin().x());
            hotspot.setY(hotspot.y() * (display_mt->render_plane().rect().height() + display_mt->banner_height()) / input_rect.height() + display_mt->origin().y() + desktop_mt->origin().y());
        }
    }

    // Keep input in the input_plane.
    clamp_point(input_rect, hotspot);
    // Ask window_manager for extra clamping.
    rect_t wm_clamp = m_wm.clamp_rect(uuid, hotspot, is_touch ? touch_down : mouse_down);
    clamp_point(wm_clamp, hotspot);

    if(!m_active_sink) {
        return;
    }

    translated = m_wm.map_event_to_guest(uuid, hotspot, is_touch ? touch_down : mouse_down, m_active_sink->absolute_enabled(), is_touch);

    // Scale touch input based on the logical width and height.
    if (is_touch && translated.size.width() != 0 && translated.size.height() != 0) {
        translated.point.setX(translated.point.x() * XT_LOGICAL_SCREEN_WIDTH / translated.size.width());
        translated.point.setY(translated.point.y() * XT_LOGICAL_SCREEN_HEIGHT / translated.size.height());
    }

    vg_debug() << "pv_vm_input::transformevent type= " << event->type 
               << " mousedown= " << mouse_down 
               << " hotspot= " << m_wm.input_plane().hotspot().x() << ", " << m_wm.input_plane().hotspot().y()
               << " translated x,y: " << translated.point.x() << "," << translated.point.y() 
               << " translated size w,h: " << translated.size.width() << "," << translated.size.height()
               << " translated status: " << translated.translate_status;

     switch (translated.translate_status)
     {
        case TRANSLATE_SUCCESS:
        {
             switch ( event->type )
             {
                 case XT_TYPE_RELATIVE:
                 case XT_TYPE_ABSOLUTE:
                     event->data.absolute.absX    = translated.point.x();
                     event->data.absolute.absY    = translated.point.y();
                     event->data.absolute.absXMax = translated.size.width();
                     event->data.absolute.absYMax = translated.size.height();
                     event->type = XT_TYPE_ABSOLUTE;
                     break;
                 case XT_TYPE_TOUCH_DOWN:
                 case XT_TYPE_TOUCH_UP:
                 case XT_TYPE_TOUCH_MOVE:
                     event->data.touch.absX = translated.point.x();
                     event->data.touch.absY = translated.point.y();
                     break;
                case XT_TYPE_TOOL_TIP:
                case XT_TYPE_TOOL_AXIS:
                case XT_TYPE_TOOL_PROXIMITY:
                     event->data.tablet_tool.absX = translated.point.x();
                     event->data.tablet_tool.absY = translated.point.y();
                     break;
                 default:
	             // Error condition? Drop the input event
	             event->type = XT_TYPE_NULL;
                 break;
             }
        }
        break;
        case TRANSLATE_SOLARIS_HACK:
        {
	   event->type = XT_TYPE_RELATIVE;
        }
        break;
        case TRANSLATE_FAIL:
        { 
	   event->type = XT_TYPE_NULL;
        }
        break;
        default:
	{
           event->type = XT_TYPE_NULL;
	}
  }
}

void
pv_vm_input_t::input_event_slot(xt_input_event event)
{
    if(event.keyCode == BTN_LEFT) {
        if(event.flags) {
            mouse_down = true;
        } else {
            mouse_down = false;
        }
    }

    if(event.type == XT_TYPE_KEY || event.type == XT_TYPE_RELATIVE)
    {
        UPDATE_KEY_MASK(m_key_state, event);
    }

    transform_event(&event);

    if(event.type == XT_TYPE_NULL) {
        return;
    }

    forward_to_active_sinks(event);
}

void
pv_vm_input_t::lost_focus()
{
    xt_input_event event;
    int i = 0;

    memset(&event, 0x00, sizeof(xt_input_event));
    for(i = 0; i < KEY_MAX; i++)
    {
        if(CHECK_BIT_SET(m_key_state, i))
        {
            event.keyCode = i;
            event.flags = 0;
            event.type = XT_TYPE_KEY;
            if(i >= BTN_LEFT && i <= BTN_EXTRA) {
                event.type = XT_TYPE_RELATIVE;
                event.keyCode = i;
            }

            forward_to_active_sinks(event);

            CLEAR_KEY(m_key_state, event);

            mouse_down = false;
        }
    }

    //Send extra wheel relZ event  ( why?! 
    event.keyCode = BTN_WHEEL;
    event.flags = 0;
    event.type = XT_TYPE_RELATIVE;
    event.data.relative.relZ = 0;
    forward_to_active_sinks(event);

    // Turn off all led codes
    emit set_input_led_code(0);
}

void pv_vm_input_t::got_focus()
{
    emit set_input_led_code(m_led_code);

    // Reprocess the mouse for the current VM
    xt_input_event event;
    memset(&event, 0x00, sizeof(xt_input_event));
    event.keyCode = 0;
    event.flags = 0;
    event.type = XT_TYPE_RELATIVE;
    forward_to_active_sinks(event);
}

void pv_vm_input_t::wake_up_toggle()
{
    xt_input_event event;

    memset(&event, 0x00, sizeof(xt_input_event));
    event.keyCode = KEY_LEFTCTRL;
    event.flags = 1;
    event.type = XT_TYPE_KEY;

    forward_to_active_sinks(event);

    event.flags = 0;

    forward_to_active_sinks(event);
}

void pv_vm_input_t::forward_to_active_sinks(const xt_input_event &event)
{
    if (m_active_sink)
        m_active_sink->enqueue_input_event(event);
    else
        vg_warning() << DTRACE << "Attempted to forward event with no active sinks.";
}

void pv_vm_input_t::vkbd_connection_status(bool is_connected)
{
    if (is_connected)
        m_active_sink = m_vkbd_sink;
    else
        m_active_sink = m_qemu2_sink;
}
