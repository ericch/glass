//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef INPUTCONFIGPARSER__H
#define INPUTCONFIGPARSER__H

#include <xt_input_global.h>
#include <inputserverfilter.h>
#include <json.hpp>
#include <input_server.h>
#include <filters/keystrokefilter.h>
#include <filters/idlefilter.h>

using json = nlohmann::json;
class input_server_t;

class input_config_parser_t : public QObject
{
    Q_OBJECT

public:

    input_config_parser_t(json &obj, input_server_t *input_server);
    virtual ~input_config_parser_t(void);
    void create_filters_from_config(void);
    QList<QString> parse_whitelist(void);
    static QPair<KeyMask, KeyMask> get_base_secondary_pair(json entry);

private:

    std::shared_ptr<input_server_filter_t> create_switcher_filter(json action);
    std::shared_ptr<input_server_filter_t> create_slot_filter(json action);
    std::shared_ptr<input_server_filter_t> create_screenshot_filter(json action);
    std::shared_ptr<input_server_filter_t> create_idle_filter(json action);
    std::shared_ptr<input_server_filter_t> create_long_press_filter(json action);
    std::shared_ptr<input_server_filter_t> create_brightness_filter(json action);
    std::shared_ptr<input_server_filter_t> create_exit_filter(json action);
    std::shared_ptr<input_server_filter_t> create_revert_to_cloned_mode_filter(json actionj);

    json &m_config;
    input_server_t *m_input_server;

};

#endif // INPUTCONFIGPARSER__H
