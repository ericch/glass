//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef HIDE_SWITCH_ACTION__H
#define HIDE_SWITCH_ACTION__H

#include <inputaction.h>
#include <glass_types.h>

class hide_switch_action_t : public input_action_t
{
    Q_OBJECT

public:

    hide_switch_action_t();
    ~hide_switch_action_t(void);

    void operator()();

public slots:

    //void vm_changed(std::shared_ptr<vm_base_t> highlight);
    void vm_changed(uuid_t highlight);

signals:

    void hide_switcher(uuid_t new_focus);
  //  void hide_vm_switcher(void);

private:

    //std::shared_ptr<vm_t> m_vm;
    uuid_t m_vm;

};

#endif //HIDE_SWITCH_ACTION__H
