//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef SWITCH_VM_ACTION__H
#define SWITCH_VM_ACTION__H

#include <inputaction.h>

class switch_vm_action_t : public input_action_t
{

    Q_OBJECT

public:

    switch_vm_action_t(const int32_t slot);
    ~switch_vm_action_t(void);

    void operator()();

signals:

    void switch_vm(const int32_t slot);

private:

    int32_t m_slot;
};

#endif //SWITCH_VM_ACTION__H
