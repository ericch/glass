//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <string>
#include <brightnessaction.h>

brightness_action_t::brightness_action_t(std::string direction) : m_increase(direction == "up") {}
brightness_action_t::~brightness_action_t(void) {}

void
brightness_action_t::operator()()
{
    if (m_increase)
    {
        emit increase_brightness();
    }
    else
    {
        emit decrease_brightness();
    }

    return;
}
