//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef QEMU_SINK__H
#define QEMU_SINK__H

#include <guestinputsink.h>
#include <vm.h>
#include <linux/input.h>
#include <libivc.h>

#define QEMU_INPUT_PORT 1588

static const uint8_t evdev_keycode_to_pc_keycode[KEY_MAX] =
{
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x00 - 0x07 */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x08 - 0x0F */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x10 - 0x17 */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x18 - 0x1F */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x20 - 0x27 */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x28 - 0x2F */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x30 - 0x37 */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x38 - 0x3F */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x40 - 0x47 */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x48 - 0x4F */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x50 - 0x57 */
    0, 0, 0, 0, 0, 0, 0, 0, /* 0x58 - 0x5F */
    0x1c, /* KP_ENTER */    /* 0x60 */
    0x1d, /* RIGHT_CTRL */  /* 0x61 */
    0x35, /* KP_SLASH */    /* 0x62 */
    0x37, /* SYSREQ */      /* 0x63 */
    0x38, /* RIGHT_ALT */   /* 0x64 */
    0,                      /* 0x65 */
    0x47, /* HOME */        /* 0x66 */
    0x48, /* UP */          /* 0x67 */
    0x49, /* PAGE_UP */     /* 0x68 */
    0x4b, /* LEFT */        /* 0x69 */
    0x4d, /* RIGHT */       /* 0x6A */
    0x4f, /* END */         /* 0x6B */
    0x50, /* DOWN */        /* 0x6C */
    0x51, /* PAGE_DOWN */   /* 0x6D */
    0x52, /* INSERT */      /* 0x6E */
    0x53, /* DELETE */      /* 0x6F */
    0, 0, 0, 0, 0, 0, 0,    /* 0x70 - 0x76 */
    0xFF, /* PAUSE_BREAK */ /* 0x77 */ // <- requires special sequence
    0, 0, 0, 0, 0,          /* 0x78 - 0x7C */
    0x5b, /* LEFT_WIN */    /* 0x7D */
    0x5c, /* RIGHT_WIN */   /* 0x7E */
    0x5d  /* DIALOG */      /* 0x7F */
};

#define SCANCODE_KEYDOWN_MASK 0x0000007f
#define SCANCODE_KEYUP_MASK   0x00000080

/*
    Returns true if the scancode requires an extended byte sent before
    the actual scan code
*/
inline bool evdev_keycode_to_qemu_scancode(xt_input_event *event)
{
    uint8_t scancode = 0;

    if(event->keyCode > 0x7f) return false;

    scancode = evdev_keycode_to_pc_keycode[event->keyCode];

    if(scancode)
    {
        event->keyCode = (uint32_t)scancode;

        if(scancode == 0xff)
        {
            // Don't key mask pause breaks here, because there is a 3-byte sequence
            // that needs to be sent. Instead, set it to a weird value, and handle
            // that value inside the actual eventSlot();
            return true;
        }
    }

    if(event->flags == 1)
    {
        event->keyCode &= SCANCODE_KEYDOWN_MASK;
    }
    else if(event->flags == 0)
    {
        event->keyCode |= SCANCODE_KEYUP_MASK;
    }

    return scancode ? true : false;
}

class qemu_stubdom_sink_t : public guest_input_sink_t
{
  Q_OBJECT
  using domid_t = int16_t;
public:
  qemu_stubdom_sink_t(domid_t domid);
  ~qemu_stubdom_sink_t();

  void set_inactive();
  void set_active();

public slots:
  void enqueue_input_event(xt_input_event event);

private:
  void handle_pause_break_key(xt_input_event event);
  void inject_extended_prefix(xt_input_event event);
  QPoint scale_absolute_event(const xt_input_event &event);

  uint32_t m_domid;
  bool m_active;

  libivc_server *m_input_server;
  libivc_client *m_input_channel;
  
  QPoint m_device_abs_max;
};

#endif // QEMU_SINK__H
