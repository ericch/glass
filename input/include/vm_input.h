//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VM_INPUT__H
#define VM_INPUT__H

#include <glass_types.h>

#include <QObject>
#include <vm_base.h>
#include <xt_input_global.h>
#include <input_plane.h>

struct vm_base_t;

class vm_input_t : public QObject
{
    Q_OBJECT;

public:

    vm_input_t(std::shared_ptr<vm_base_t> vm) : m_base(vm) {}
    virtual ~vm_input_t() = default;
    virtual std::shared_ptr<vm_base_t> base() { return m_base; }

public slots:

    virtual void input_event_slot(xt_input_event event) = 0;
    virtual void lost_focus() = 0;
    virtual void got_focus() = 0;
    virtual void wake_up_toggle() = 0;
    virtual void keyboard_led_changed(const uint32_t led_code) = 0;

signals:
 void update_hotspot(display_plane_t *display, point_t point, bool grabbed, uuid_t uuid);
 void set_input_led_code(int32_t led_code);

protected:

    std::shared_ptr<vm_base_t> m_base;
};

#endif // VM_RENDER__H
